package com.alpha.txlcn;

import com.codingapi.txlcn.tm.config.EnableTransactionManagerServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author JavaAlpha
 * @Description: 分布式事务服务
 * @date: 2020/8/19 0019 上午 9:46
 */
@SpringBootApplication
@EnableTransactionManagerServer
public class TxlcnServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(TxlcnServerApplication.class, args);
		System.out.println("分布式事务服务启动成功");
	}
}