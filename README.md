# 基于最新稳定版本Spring Boot 2.3.3.RELEASE 搭建Tx-LCN分布式事务
## 基于Eureka注册中心和SpringBootAdmin监控中心 

[![Fork me on Gitee](https://gitee.com/javaalpha/alpha-txlcn-demo/widgets/widget_3.svg)](https://gitee.com/javaalpha/alpha-txlcn-demo)

[![javaalpha/alpha-txlcn-demo](https://gitee.com/javaalpha/alpha-txlcn-demo/widgets/widget_card.svg?colors=393222,ebdfc1,fffae5,d8ca9f,393222,a28b40)](https://gitee.com/javaalpha/alpha-txlcn-demo)

## 技术版本
* SpringBoot 2.3.3.RELEASE
* SpringCloud Hoxton.SR7
* Tx-LCN 5.0.2.RELEASE
* knife4j 2.0.4
* SpringBootAdmin 2.3.0

## 运行说明
* 1.新建数据库txlcn-demo，导入txlcn-demo.sql
* 2.依次启动txlcn-admin, txlcn-eureka，txlcn-server，txlcn-provider，txlcn-consumer

### 服务调用方 txlcn-consumer
* 接口地址：http://localhost:1112/doc.html
* http://localhost:1112/customer/update

### 被调用方 txlcn-provider
接口地址：http://localhost:1113/doc.html

### 事务管理平台
http://localhost:1111/ 密码123456

## 说明
* 一、txlcn-server 分布式事务管理端

_**注意：添加application.properties 空文件，或者是直接在里面配置**_

* 1.pom添加：
```xml
<dependency>
    <groupId>com.codingapi.txlcn</groupId>
    <artifactId>txlcn-tm</artifactId>
    <version>${txlcn.version}</version>
</dependency>
```
* 2.TxlcnServerApplication 启动类添加注解，启用分布式事务：
```java
@EnableTransactionManagerServer
```

* 二、txlcn-consumer 分布式事务使用
* 1.pom添加：
```xml
<dependency>
    <groupId>com.codingapi.txlcn</groupId>
    <artifactId>txlcn-tc</artifactId>
    <version>${txlcn.version}</version>
</dependency>
<dependency>
    <groupId>com.codingapi.txlcn</groupId>
    <artifactId>txlcn-txmsg-netty</artifactId>
    <version>${txlcn.version}</version>
</dependency>
```
* 2.TxlcnConsumerApplication 启动类添加注解，启用分布式事务：
```java
@EnableTransactionManagerServer
```

* 三、分布式事务注解说明
* 1.调用方server实现类添加注解
```java
@LcnTransaction(propagation = DTXPropagation.REQUIRED) //分布式事务注解(调用方)
```
* 2.被调用方server实现类添加注解
```java
@LcnTransaction(propagation = DTXPropagation.SUPPORTS) //分布式事务注解(参与方)
```