package com.alpha.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAdminServer
@SpringBootApplication
public class TxlcnAdminApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TxlcnAdminApplication.class, args);
		System.out.println("Admin 服务启动成功");
	}
	
}
