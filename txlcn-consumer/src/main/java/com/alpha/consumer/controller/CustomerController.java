package com.alpha.consumer.controller;

import com.alpha.consumer.pojo.Customer;
import com.alpha.consumer.service.CustomerService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 15:02:00
 */
@Api(tags = "客户功能接口")
@RestController
@RequestMapping("/customer")
public class CustomerController {
	@Autowired
	private CustomerService customerService;
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "接口返回成功状态"),
			@ApiResponse(code = 500, message = "接口返回未知错误，请联系开发人员调试")
	})
	@ApiOperation(value = "添加接口", notes = "访问此接口，添加客户")
	@PostMapping("/add")
	public String add(@RequestBody Customer customer){
		int row = customerService.insertSelective(customer);
		if (row > 0){
			return "success";
		}else {
			return "error";
		}
	}
	
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id",value = "客户ID",required = true,dataType = "Integer",paramType = "path")
	})
	@ApiOperation(value = "删除接口", notes = "访问此接口，删除客户")
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Integer id){
		int row = customerService.deleteByPrimaryKey(id);
		if (row > 0){
			return "success";
		}else {
			return "error";
		}
	}
	
	@ApiImplicitParams({
			@ApiImplicitParam(name = "name",value = "客户名",required = true,dataType = "String"),
			@ApiImplicitParam(name = "mobile",value = "手机号",required = true,dataType = "String"),
			@ApiImplicitParam(name = "id",value = "客户ID",required = true,dataType = "Integer")
	})
	@ApiOperation(value = "更新接口", notes = "访问此接口，更新客户")
	@PostMapping("/update")
	public String update(@RequestParam("name") String name, @RequestParam("mobile") String mobile, @RequestParam("id") Integer id){
		Customer customer = customerService.selectByPrimaryKey(id);
		if (null == customer){
			return "id 错误，顾客不存在";
		}
		int row = customerService.updateByPrimaryKeySelective(customer, name, mobile);
		if (row > 0){
			return "success";
		}else {
			return "error";
		}
	}
	
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id",value = "客户ID",required = true,dataType = "Integer",paramType = "path")
	})
	@ApiOperation(value = "查询接口", notes = "访问此接口，查询客户")
	@GetMapping("/getById/{id}")
	public Customer get(@PathVariable Integer id){
		return customerService.selectByPrimaryKey(id);
	}
}
