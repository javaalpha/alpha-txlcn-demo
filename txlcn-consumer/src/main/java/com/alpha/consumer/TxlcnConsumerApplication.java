package com.alpha.consumer;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description： 分布式事务调用方
 * @Author： YDF
 * @Date： 2020/8/19 0019 
 */
@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
@EnableDistributedTransaction
@MapperScan("com.alpha.consumer.mapper")
public class TxlcnConsumerApplication {
	public static void main(String[] args) {
		SpringApplication.run(TxlcnConsumerApplication.class, args);
		System.out.println("分布式事务调用方启动成功");
	}
}
