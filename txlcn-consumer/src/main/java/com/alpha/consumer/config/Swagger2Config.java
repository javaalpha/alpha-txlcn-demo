package com.alpha.consumer.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author YDF
 * @version 1.0.0
 * @Description 访问http://localhost:port/doc.html
 * @createTime 2020年08月19日 15:43:00
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
//@EnableSwaggerBootstrapUi
public class Swagger2Config {
	
	/**
	 * 创建连接的包信息
	 * <p>
	 * 配置统一返回的controller路径RequestHandlerSelectors.basePackage
	 *
	 * @return 返回创建状况
	 */
	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.useDefaultResponseMessages(false)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.alpha.consumer.controller"))
				.paths(PathSelectors.any())
				.build();
		
	}
	
	
	/**
	 * 设置文档信息主页的内容说明
	 *
	 * @return 文档信息
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Project textBook API ")
				.description("消费者服务接口")
				.termsOfServiceUrl("http://localhost:1112/")
				.contact(new Contact("JavaAlpha", "http://localhost:1112/", "alpha@qq.com"))
				.license("PLE")
				.version("1.0")
				.build();
	}
	
}