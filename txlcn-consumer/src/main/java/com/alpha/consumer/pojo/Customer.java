package com.alpha.consumer.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * customer
 * @author 
 */
@Data
@ApiModel("客户对象")
public class Customer implements Serializable {
    private Integer id;
    
    @ApiModelProperty(required = true, notes = "用户名")
    private String name;
    
    @ApiModelProperty(required = true, notes = "年龄")
    private Integer age;
    
    @ApiModelProperty(required = true, notes = "性别")
    private String sex;
    
    @ApiModelProperty(required = true, notes = "手机号")
    private String mobile;

    private static final long serialVersionUID = 1L;
}