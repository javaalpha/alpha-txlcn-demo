package com.alpha.consumer.pojo;

import java.io.Serializable;

import lombok.*;

/**
 * student
 * @author
 */
//@Builder
@Getter
@Setter
@NoArgsConstructor
public class Student implements Serializable {
    private Integer id;

    private String name;

    private String mobile;

    private Integer age;

    private String pwd;
}
