package com.alpha.consumer.client;

import com.alpha.consumer.pojo.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 17:08:00
 */
//@FeignClient(value = "txlcn-provider", fallback = StudentServerFeignFallBack.class)
@FeignClient(value = "txlcn-provider", fallbackFactory = StudentServerFeignFallBack.class)
public interface StudentServerFeign {
	
	@PostMapping("/student/add")
	String add(@RequestBody Student student);
	
	@GetMapping("/student/delete/{id}")
	String delete(@PathVariable Integer id);
	
	@PostMapping("/student/update")
	String update(@RequestParam("name") String name, @RequestParam("mobile") String mobile, @RequestParam("id") Integer id);
	
	@GetMapping("/student/getById/{id}")
	Student get(@PathVariable Integer id);
}
