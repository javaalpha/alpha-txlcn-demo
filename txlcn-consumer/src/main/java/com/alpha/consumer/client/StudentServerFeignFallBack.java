package com.alpha.consumer.client;

import com.alpha.consumer.pojo.Student;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.support.FallbackCommand;
import org.springframework.stereotype.Component;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 17:56:00
 */
@Component
public class StudentServerFeignFallBack implements FallbackFactory<StudentServerFeign> {
	@Override
	public StudentServerFeign create(Throwable throwable) {
		return new StudentServerFeign(){
			@Override
			public String add(Student student) {
				return null;
			}
			
			@Override
			public String delete(Integer id) {
				return null;
			}
			
			@Override
			public String update(String name, String mobile, Integer id) {
				return null;
			}
			
			@Override
			public Student get(Integer id) {
				return null;
			}
		};
	}
}
