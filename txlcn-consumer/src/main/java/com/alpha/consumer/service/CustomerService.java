package com.alpha.consumer.service;

import com.alpha.consumer.pojo.Customer;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 14:37:00
 */
public interface CustomerService {
	int deleteByPrimaryKey(Integer id);
	
	int insert(Customer record);
	
	int insertSelective(Customer record);
	
	Customer selectByPrimaryKey(Integer id);
	
	int updateByPrimaryKeySelective(Customer record, String name, String mobile);
	
	int updateByPrimaryKey(Customer record);
}
