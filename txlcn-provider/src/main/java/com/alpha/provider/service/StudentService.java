package com.alpha.provider.service;

import com.alpha.provider.pojo.Student;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 14:47:00
 */
public interface StudentService {
	int deleteByPrimaryKey(Integer id);
	
	int insert(Student record);
	
	int insertSelective(Student record);
	
	Student selectByPrimaryKey(Integer id);
	
	int updateByPrimaryKeySelective(Student record);
	
	int updateByPrimaryKey(Student record);
}
