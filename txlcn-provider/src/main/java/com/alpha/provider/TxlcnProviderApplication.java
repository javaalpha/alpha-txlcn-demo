package com.alpha.provider;

import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Description： 分布式事务提供方
 * @Author： YDF
 * @Date： 2020/8/19 0019
 */
@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient
@EnableDistributedTransaction
@MapperScan("com.alpha.provider.mapper")
public class TxlcnProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(TxlcnProviderApplication.class, args);
		System.out.println("分布式事务提供方启动成功");
	}
}