package com.alpha.provider.controller;

import com.alpha.provider.pojo.Student;
import com.alpha.provider.service.StudentService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author YDF
 * @version 1.0.0
 * @Description TODO
 * @createTime 2020年08月19日 15:02:00
 */
@Api(tags = "学生功能接口")
@RestController
@RequestMapping("/student")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "接口返回成功状态"),
			@ApiResponse(code = 500, message = "接口返回未知错误，请联系开发人员调试")
	})
	@ApiOperation(value = "添加接口", notes = "访问此接口，添加学生")
	@PostMapping("/add")
	public String add(@RequestBody Student student){
		int row = studentService.insert(student);
		if (row > 0){
			return String.valueOf(student.getId());
		}
		return null;
//		if (row > 0){
//			return "success";
//		}else {
//			return "error";
//		}
	}
	
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id",value = "学生ID",required = true,dataType = "Integer",paramType = "path")
	})
	@ApiOperation(value = "删除接口", notes = "访问此接口，删除学生")
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Integer id){
		int row = studentService.deleteByPrimaryKey(id);
		if (row > 0){
			return "success";
		}else {
			return "error";
		}
	}
	
	@ApiImplicitParams({
			@ApiImplicitParam(name = "name",value = "学生名",required = true,dataType = "String"),
			@ApiImplicitParam(name = "mobile",value = "手机号",required = true,dataType = "String"),
			@ApiImplicitParam(name = "id",value = "学生ID",required = true,dataType = "Integer")
	})
	@ApiOperation(value = "更新接口", notes = "访问此接口，更新学生")
	@PostMapping("/update")
	public String update(@RequestParam("name") String name, @RequestParam("mobile") String mobile, @RequestParam("id") Integer id){
		Student student = studentService.selectByPrimaryKey(id);
		if (null == student){
			return "id 错误，顾客不存在";
		}
		student.setName(name);
		student.setMobile(mobile);
		int row = studentService.updateByPrimaryKeySelective(student);
		if (row > 0){
			return "success";
		}else {
			return "error";
		}
	}
	
	@ApiOperation(value = "查询接口", notes = "访问此接口，根据ID查询学生")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id",value = "学生ID",required = true,dataType = "Integer",paramType = "path")
	})
	@GetMapping("/getById/{id}")
	public Student get(@PathVariable Integer id){
		return studentService.selectByPrimaryKey(id);
	}
}
