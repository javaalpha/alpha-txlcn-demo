package com.alpha.provider.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * student
 * @author 
 */
@Data
public class Student implements Serializable {
    private Integer id;

    private String name;

    private String mobile;

    private Integer age;

    private String pwd;

    private static final long serialVersionUID = 1L;
}