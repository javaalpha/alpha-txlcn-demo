package com.alpha.provider.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * customer
 * @author 
 */
@Data
public class Customer implements Serializable {
    private Integer id;

    private String name;

    private Integer age;

    private String sex;

    private String mobile;

    private static final long serialVersionUID = 1L;
}