package com.alpha.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @Description： Eureka注册中心
 * @Author： JavaAlpha
 * @Date： 2020/8/19 0019
 */
@EnableEurekaServer
@SpringBootApplication
public class TxlcnEurekaApplication {
	public static void main(String[] args) {
		SpringApplication.run(TxlcnEurekaApplication.class, args);
		System.out.println("Eureka注册中心启动成功");
	}
}